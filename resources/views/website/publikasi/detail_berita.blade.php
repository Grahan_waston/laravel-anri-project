@extends('website.header_footer')

@section('content')
    <!-- Page Header Start -->
    <div class="container-fluid header-faq py-5 mb-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="container text-center py-5">
            <h1 class="display-4 text-white animated slideInDown mb-3">Program Diklat Detail</h1>
            <nav aria-label="breadcrumb animated slideInDown">
                <ol class="breadcrumb justify-content-center mb-0">
                    <li class="breadcrumb-item"><a class="text-white" href="#">Beranda</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="#">Diklat</a></li>
                    <li class="breadcrumb-item text-primary active" aria-current="page">Program Diklat</li>
                </ol>
            </nav>
        </div>
    </div>
    <!-- Page Header End -->
    <section class="wrapper py-5">
        <div class="container py-5">
            <div class="card">
                <div class="card-header">
                    <h2 class="h4 my-2">Diklat Fungsional Pengangkatan Arsiparis Tingkat Ahli Angkatan I 2021 (PNBP)</h2>
                </div>
                <div class="card-body">
                    <h6 class="mb-2 fw-normal badge bg-secondary">
                        DFK.01.01.2022
                    </h6>
                    <h6 class="mb-2 fw-normal badge bg-primary">
                        PNBP Fungsional
                    </h6>
                    <div class="classic-view my-3">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td>Tanggal</td>
                                        <td>08/02/2021 s.d 26/03/2021</td>
                                    </tr>
                                    <tr>
                                        <td>Tempat</td>
                                        <td>Distance Learning ( Diklat Jarak Jauh )<br></td>
                                    </tr>
                                    <tr>
                                        <td>Biaya</td>
                                        <td>Rp. 6790000</td>
                                    </tr>
                                    <tr>
                                        <td>Durasi</td>
                                        <td>280 Jam Pelajaran</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:middle ;">File Pendukung</td>
                                        <td class="d-flex justify-content-between align-items-center">
                                            <p class="mb-0">Diklat Fungsional Pengangkatan Arsiparis.pdf</p>
                                            <a href="#" class="btn btn-primary">Download</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <h3 class="mb-3">Deskripsi</h3>
                    <p class="text-justify mb-5">Diklat ini bertujuan untuk memberikan bekal bagi Sumber Daya Manusia (SDM)
                        di bidang kearsipan. Di dalam kelas, peserta diklat akan mempelajari tentang teori kearsipan, serta
                        pemahaman tentang penyelenggaraan kearsipan, termasuk pengelolaan arsip baik arsip dinamis maupun
                        arsip statis, mulai dari tahap penciptaan, penggunaan, pemeliharaan, dan penyusutan, sampai pada
                        tahap pengelolaan statis. Selain itu, peserta diklat mendapatkan pengetahuan metode penelitian dan
                        teknik penulisan ilmiah yang dapat membantu dalam pembuatan penulisan karya tulis ilmiah dan
                        pembuatan manual kearsipan, serta pemahaman tentang jabatan fungsional Arsiparis dan angka kredit.
                        Kemudian, di instansi masing-masing, peserta diklat melakukan program aktualisasi yang dibuktikan
                        dengan kegiatan magang untuk menerapkan pembelajaran di kelas. Pada sesi akhir diklat, peserta
                        diwajibkan membuat laporan hasil magang dan dikirimkan paling lambat 7 (tujuh) hari kalender setelah
                        kegiatan magang berakhir. Setelah mengikuti diklat ini, peserta diharapkan mampu mengaplikasikan
                        pengetahuan, keterampilan, dan sikap sebagai Pejabat Fungsional Arsiparis Tingkat Ahli.</p>
                    <h3 class="mb-3">Syarat Peserta</h3>
                    <p class="text-justify mb-5">
                        1. Usia pada saat pembukaan diklat maksimal 53 atau 56 tahun bagi peserta yang sudah melalui proses
                        inpassing;<br>
                        2. Pendaftaran hanya melalui E-Registrasi (pusdiklat.anri.go.id);<br>
                        3. Dokumen (hasil scan) yang diunggah dalam format pdf (maksimal 2 MB/dokumen) dengan format “nama
                        dokumen_nama pendaftar” (misalnya KTP_Widya Pratiwi):<br>
                        a. Kartu Tanda Penduduk (KTP);<br>
                        b. Ijazah minimal D-IV atau Sarjana Strata 1 (S1) selain kearsipan;<br>
                        c. Surat Keputusan (SK) Pangkat Terakhir, paling rendah Penata Muda, Golongan/Ruang III/a;<br>
                        d. Surat rekomendasi/persetujuan dan komitmen dari pimpinan instansi yang bersangkutan bahwa calon
                        peserta akan diangkat menjadi Arsiparis setelah dinyatakan lulus diklat, dibuktikan dengan
                        melampirkan surat rekomendasi dan komitmen dari atasan;<br>
                        e. SK Pengangkatan Arsiparis Tingkat Ahli dari instansi atau surat rekomendasi pengangkatan dari
                        Kepala Arsip Nasional Republik Indonesia, bagi calon peserta yang sudah diangkat atau sedang dalam
                        proses pengangkatan Arsiparis melalui program inpassing;<br>
                        f. Surat Keterangan Dokter dan Surat Pernyataan Tanggung Jawab mutlak ditandatangani di atas materai
                        bagi peserta yang sedang mengandung/hamil.<br>
                        4. Wajib mengikuti diklat sampai selesai.
                </div>
            </div>
        </div>
    </section>
@endsection
