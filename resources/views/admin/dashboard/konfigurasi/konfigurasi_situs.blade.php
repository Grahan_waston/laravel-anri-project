@extends('admin.main')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        Konfigurasi Situs
                    </h2>
                </div>
                <div class="col-auto d-print-none">

                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row">
                <form action="/konfigurasi-situs/{{ $website->id }}" id="form" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="col-md-8 mb-4 mb-md-0">
                        @if (session('success'))
                            <div class="alert alert-success d-flex align-items-center" role="alert">
                                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img"
                                    aria-label="Danger:">
                                    <use xlink:href="#exclamation-triangle-fill" />
                                </svg>
                                <div class="text-center">
                                    {{ session('success') }}
                                </div>
                            </div>
                        @endif
                        <div class="card mb-3">
                            <div class="card-header">
                                <h2 class="page-title">
                                    Konfigurasi Website
                                </h2>
                            </div>
                            <div class="card-body">
                                <div class="mb-3">
                                    <div class="form-label">Nama Website</div>
                                    <input type="text" class="form-control" id="nama_website" name="nama_website"
                                        value="{{ $website->nama_website }}" required>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Alamat</div>
                                    <textarea name="alamat" id="alamat" cols="30" rows="5" class="form-control" required>{{ $website->alamat }}</textarea>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">No Telepon</div>
                                    <input type="text" class="form-control" id="no_telfon" name="no_telfon"
                                        value="{{ $website->no_telfon }}" required>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">No Whatsapp</div>
                                    <input type="text" class="form-control" id="no_whatsapp" name="no_whatsapp"
                                        value="{{ $website->no_whatsapp }}" required>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">E-mail 1</div>
                                    <input type="e-mail" class="form-control" id="email_pertama" name="email_pertama"
                                        value="{{ $website->email_pertama }}" required>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">E-mail 2</div>
                                    <input type="e-mail" class="form-control" id="email_kedua" name="email_kedua"
                                        value="{{ $website->email_kedua }}" required>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Embed Maps</div>
                                    <textarea name="maps" id="maps" cols="50" rows="5" class="form-control" required>{!! $website->maps !!}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 mb-4 mb-md-0">
                        <div class="card mb-3">
                            <div class="card-header">
                                <h2 class="page-title">
                                    Sosial Media
                                </h2>
                            </div>
                            <div class="card-body">
                                <div class="mb-3">
                                    <div class="form-label">Facebook</div>
                                    <input type="text" class="form-control" id="facebook" name="facebook"
                                        value="{{ $website->facebook }}" required>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Twitter</div>
                                    <input type="text" class="form-control" id="twitter" name="twitter"
                                        value="{{ $website->twitter }}" required>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Instagram</div>
                                    <input type="text" class="form-control" id="instagram" name="instagram"
                                        value="{{ $website->instagram }}" required>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Youtube</div>
                                    <input type="text" class="form-control" id="youtube" name="youtube"
                                        value="{{ $website->youtube }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 mb-4 mb-md-0">
                        <div class="card mb-3">
                            <div class="card-header">
                                <h2 class="page-title">
                                    Quick Link
                                </h2>
                            </div>
                            <div class="card-body">
                                <div class="mb-3">
                                    <div class="form-label">Quick Link 1</div>
                                    <input type="text" class="form-control m-2" placeholder="Masukkan Nama"
                                        value="{{ $link[0]->nama }}" required>
                                    <input type="text" class="form-control m-2" placeholder="Masukkan Alamat URL"
                                        value="{{ $link[0]->url }}" required>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Quick Link 2</div>
                                    <input type="text" class="form-control m-2" placeholder="Masukkan Nama"
                                        value="{{ $link[1]->nama }}" required>
                                    <input type="text" class="form-control m-2" placeholder="Masukkan Alamat URL"
                                        value="{{ $link[1]->url }}" required>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Quick Link 3</div>
                                    <input type="text" class="form-control m-2" placeholder="Masukkan Nama"
                                        value="{{ $link[2]->nama }}" required>
                                    <input type="text" class="form-control m-2" placeholder="Masukkan Alamat URL"
                                        value="{{ $link[2]->url }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 mb-4 mb-md-0">
                        <div class="card mb-3">
                            <div class="card-header">
                                <h2 class="page-title">
                                    Video Unggulan
                                </h2>
                            </div>
                            <div class="card-body">
                                <div class="mb-3">
                                    <div class="form-label">Url Video Youtube</div>
                                    <input type="text" class="form-control" id="url_youtube" name="url_youtube"
                                        value="{{ $website->url_youtube }}" required>
                                </div>
                            </div>
                            <div class="card-footer d-flex justify-content-end bg-light">
                                <button class="btn btn-publish btn-primary">Publish</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.min.css">
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('.btn-publish').on('click', function() {
			Swal.fire({
				title: 'Simpan Konfigurasi Situs?',
				text: "Apakah anda yakin?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya',
				reverseButtons: true
			}).then((result) => {
				if (result.isConfirmed) {
					Swal.fire(
						'Success!',
						'Konfigurasi Berhasil Disimpan',
						'success',
					).then((result) => {
						window.location = "?page=dashboard";
					})
				}
			})
		});
	});
</script>

<script type="text/javascript">
	$(function() {
		$('#navbar-menu > .navbar-nav > .nav-item > .nav-link > .nav-link-title:contains("Konfigurasi Situs")').parents('.nav-item').addClass('active').find('.dropdown-menu').addClass('show').find('.dropdown-item:contains("Informasi Kontak")').addClass('active');
	});
</script> --}}
@endsection
