@extends('admin.main')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        Manajemen User
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row">
                <div class="col-md-8 mb-4 mb-md-0">
                    <form action="/manajemen-user/{{ $data_user->id }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="mb-3">
                                    <div class="form-label">Username</div>
                                    <input type="text" id="username" name="username" class="form-control"
                                        value="{{ $data_user->username }}">
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Nama</div>
                                    <input type="text" id="name" name="name" class="form-control"
                                        value="{{ $data_user->name }}">
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Role</div>
                                    <select id="role" name="role" class="form-control">
                                        <option value="admin">Admin</option>
                                        <option value="operator">Operator</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Status</div>
                                    <select name="" id="" class="form-control">
                                        <option value="">Aktif</option>
                                        <option value="">Tidak Aktif</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {
            $('#navbar-menu > .navbar-nav > .nav-item > .nav-link > .nav-link-title:contains("Manajemen User")')
                .parents('.nav-item').addClass('active');
        });
    </script>
@endsection
