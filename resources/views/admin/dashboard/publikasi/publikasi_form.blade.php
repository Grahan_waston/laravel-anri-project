@extends('admin.main')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        Tambah Publikasi
                    </h2>
                </div>
                <div class="col-auto d-print-none">
                    <!-- <div class="btn-list">
             <a href="?page=berita-form" class="btn btn-primary d-inline-block">
              <i class="fa fa-plus me-2"></i> Buat Berita Baru
             </a>
            </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row">
                <form action="/publikasi" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-8 mb-4 mb-md-0">
                        <div class="mb-3">
                            <input type="text" class="form-control" placeholder="Add Title" id="title"
                                name="title">
                        </div>
                        <div class="mb-3">
                            <select name="category_id" id="category_id" class="form-control">
                                <option disabled selected>Pilih Kategori</option>
                                <!-- <option value="">Artikel</option> -->
                                <option value="1">Berita</option>
                                <!-- <option value="">Pengumuman</option> -->
                                <option value="2">Infografis</option>
                            </select>
                        </div>
                        <div class="permalink mb-3">
                            <span class="text-dark">Permalink : </span>
                            <a href="#">https://pusdiklat.anri.go.id/pages</a>
                        </div>
                        <div class="card border-0 shadow-none mb-3">
                            <div class="form-group">
                                <textarea class="tinymce-editor" id="editortiny" name="body"></textarea>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-header font-weight-bold">SEO</div>
                            <div class="card-body">
                                <div class="mb-3">
                                    <div class="form-label">Seo Title</div>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Seo Keyword</div>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <div class="form-label">Seo Description</div>
                                    <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card mb-3">
                            <div class="card-header font-weight-bold">Publish</div>
                            <div class="card-body">

                                <div class="mb-2">
                                    <select name="status" id="status" class="form-control">
                                        <option value="Published">Published</option>
                                        <option value="Draft">Draft</option>
                                    </select>
                                </div>
                                <div class="input-icon mb-2">
                                    <input class="form-control " placeholder="Select a date" id="datepicker-icon"
                                        value="2020-06-20" />
                                    <span class="input-icon-addon">
                                        <!-- Download SVG icon from http://tabler-icons.io/i/calendar -->
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                            viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                            stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                            <rect x="4" y="5" width="16" height="16"
                                                rx="2" />
                                            <line x1="16" y1="3" x2="16" y2="7" />
                                            <line x1="8" y1="3" x2="8" y2="7" />
                                            <line x1="4" y1="11" x2="20" y2="11" />
                                            <line x1="11" y1="15" x2="12" y2="15" />
                                            <line x1="12" y1="15" x2="12" y2="18" />
                                        </svg>
                                    </span>
                                </div>
                            </div>

                        </div>
                        <div class="card mb-3">
                            <div class="card-header font-weight-bold">Foto Utama</div>
                            <div class="card-body">
                                <div class="mb-3">
                                    <div class="form-label">Upload Image</div>
                                    <input type="file" class="form-control" id="image_main" name="image_main">
                                    <div class="form-text">
                                        File type : PNG, JPG, JPEG, rekomendasi ukuran 400x300
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-header font-weight-bold">Foto Album</div>
                            <div class="card-body">
                                <div class="mb-3">
                                    <div class="form-label">Upload Image</div>
                                    <input type="file" class="form-control" id="image_album" name="image_album"
                                        multiple>
                                    <div class="form-text">
                                        File type : PNG, JPG, JPEG, rekomendasi ukuran 400x300
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer d-flex justify-content-end bg-light">
                                <button class="btn btn-primary btn-publish">Publish</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <style type="text/css">
        [data-template] {
            display: none;
        }
    </style>
    <link rel="stylesheet" type="text/css"
        href="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.min.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.min.js"></script>


    <script type="text/javascript">
        $(function() {
            $('#navbar-menu > .navbar-nav > .nav-item > .nav-link > .nav-link-title:contains("Berita")').parents(
                '.nav-item').addClass('active');
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('.btn-publish').on('click', function() {
                Swal.fire({
                    title: 'Simpan Publikasi?',
                    text: "Apakah anda yakin?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya',
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                            'Success!',
                            'Publikasi Berhasil Disimpan',
                            'success',
                        ).then((result) => {
                            window.location = "?page=publikasi";
                        })
                    }
                })
            });
        });
    </script>
    <script src="assets/libs/litepicker/dist/litepicker.js"></script>
    <script src="assets/libs/litepicker/dist/litepicker.js"></script>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script type="text/javascript">
        const useDarkMode = window.matchMedia('(prefers-color-scheme: light)').matches;
        const isSmallScreen = window.matchMedia('(max-width: 1366)').matches;

        tinymce.init({
            selector: '#editortiny',
            theme: 'silver',
            plugins: 'preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars image link codesample table charmap pagebreak nonbreaking insertdatetime advlist lists wordcount help charmap quickbars emoticons',
            editimage_cors_hosts: ['picsum.photos'],
            menubar: 'file edit view insert format tools table help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontfamily fontsize blocks | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | image  link codesample',
            toolbar_sticky: false,
            // toolbar_sticky_offset: isSmallScreen ? 108 : 108,
            autosave_ask_before_unload: true,
            autosave_interval: '30s',
            autosave_prefix: '{path}{query}-{id}-',
            autosave_restore_when_empty: false,
            autosave_retention: '2m',
            image_advtab: true,
            importcss_append: true,
            file_picker_callback: (callback, value, meta) => {
                /* Provide file and text for the link dialog */
                if (meta.filetype === 'file') {
                    callback('https://www.google.com/logos/google.jpg', {
                        text: 'My text'
                    });
                }

                /* Provide image and alt text for the image dialog */
                if (meta.filetype === 'image') {
                    callback('https://www.google.com/logos/google.jpg', {
                        alt: 'My alt text'
                    });
                }

                /* Provide alternative source and posted for the media dialog */
                if (meta.filetype === 'media') {
                    callback('movie.mp4', {
                        source2: 'alt.ogg',
                        poster: 'https://www.google.com/logos/google.jpg'
                    });
                }
            },
            template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_class: 'mceNonEditable',
            toolbar_mode: 'sliding',
            contextmenu: 'link image table',
            // skin: useDarkMode ? 'oxide-dark' : 'oxide',
            // content_css: useDarkMode ? 'dark' : 'default',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }'
        });
    </script>
@endsection
